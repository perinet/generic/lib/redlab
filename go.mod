module gitlab.com/perinet/generic/lib/redlab

go 1.21.1

require github.com/sstallion/go-hid v0.14.1

require golang.org/x/sys v0.8.0 // indirect
