/*
 * Copyright (c) 2018-2023 Perinet GmbH
 * All rights reserved
 *
 * This software is dual-licensed: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 as
 * published by the Free Software Foundation. For the terms of this
 * license, see http://www.fsf.org/licensing/licenses/agpl-3.0.html,
 * or contact us at https://server.io/contact/ when you want license
 * this software under a commercial license.
 */

// Package redlab_1208 implements USB HID interface to Redlab 1208LS
// measurement device (https://www.meilhaus.de/en/redlab-1208.htm).
// Reference python implementation: https://github.com/wjasper/Linux_Drivers.git
package redlab_1208fs_plus

import (
	"log"
	"time"

	"github.com/sstallion/go-hid"
)

// Channel to be used by measurement
type Channel byte

// Gain to be used by measurement
type Gain byte

var (
	logger  log.Logger  = *log.Default()
	hid_dev *hid.Device = nil
)

const (
	// Cannels
	Channel_0 Channel = 0
	Channel_1 Channel = 1
	Channel_2 Channel = 2
	Channel_3 Channel = 3
	Channel_4 Channel = 4
	Channel_5 Channel = 5
	Channel_6 Channel = 6
	Channel_7 Channel = 7

	// Total number of channels
	Channel_Number uint = 8

	// Gain Ranges
	Gain_Unipolar_5_0V  	Gain = 0x08 // Unipolar 5.0 V (analog output)
	Gain_Differential_20_0V Gain = 0x00 // Differential +/- 20.0 V
	Gain_Differential_10_0V Gain = 0x10 // Differential +/- 10.0 V
	Gain_Differential_5_0V  Gain = 0x20 // Differential +/- 5.00 V
	Gain_Differential_4_0V  Gain = 0x30 // Differential +/- 4.00 V
	Gain_Differential_2_5V  Gain = 0x40 // Differential +/- 2.50 V
	Gain_Differential_2_0V  Gain = 0x50 // Differential +/- 2.00 V
	Gain_Differential_1_25V Gain = 0x60 // Differential +/- 1.25 V
	Gain_Differential_1_0V  Gain = 0x70 // Differential +/- 1.00 V

	// USB IDs
	vid uint16 = 0x09db
	pid uint16 = 0x00e8

	// Commands
	reset     byte = 0x42
	analog_in byte = 0x10

	// digital ports A and B
	porta byte = 0
	portb byte = 1

	// digital i/o commands
	dtristate 	byte = 0x00 // Read/write digital port tristate register
	dport 		byte = 0x01 // Read digital port pins / write output latch register
	dlatch		byte = 0x02 // Read/write digital port output latch register
	blink_led 	byte = 0x41
)

// Reset the measurement device
func Reset() {
	data := []byte{reset, 0, 0, 0, 0, 0, 0, 0}
	hid_dev.Write(data)
}

// Measure an analog in channel
//
// Parameters:
//   - channel: The channel to be measured (e.g. Channel_0)
//   - gain: The gain of the measurement (e.g. Gain_Differential_4_0V for a 3.3V max measurement)
//
// Returns the voltage of the measurement
func Analog_in(channel Channel, gain Gain) (float64, error) {
	data := []byte{analog_in, byte(channel), byte(gain), 0, 0, 0, 0, 0}
	_, err := hid_dev.Write(data)
	if err != nil {
		return 0.0, err
	}

	read_data := make([]byte, 3)
	_, err = hid_dev.ReadWithTimeout(read_data, time.Millisecond*100)
	if err != nil {
		return 0.0, err
	}

	var value float64
	var volts float64
	if gain == Gain_Unipolar_5_0V {
		value = float64(uint16(read_data[1])<<4 | uint16(read_data[0])&0x0f) - 0x400
		volts = float64(value) * factor(gain) / 0x2000
	} else {
		value = float64(int16((uint16(read_data[0])<<4 | uint16(read_data[1])<<8))) / 16
		volts = float64(value) * factor(gain) / 0x1000
	}
	return volts, nil
}

// Prepare dependencies for measurement
func Start() {
	logger.SetPrefix("redlab: ")
	logger.Println("starting")

	logger.Println("hid.Init()")
	err := hid.Init()
	if err != nil {
		log.Fatal(err)
	}

	logger.Println("hid.OpenFirst(", vid, ",", pid, ")")
	hid_dev, err = hid.OpenFirst(vid, pid)
	if err != nil {
		log.Fatal(err)
	}
}

// Cleanup after measurements has been finished
func Stop() error {
	return hid_dev.Close()
}

func factor(gain Gain) float64 {
	var res float64 = 0.0

	if gain == Gain_Differential_10_0V {
		res = 10.0
	} else if gain == Gain_Differential_1_0V {
		res = 1.0
	} else if gain == Gain_Differential_1_25V {
		res = 1.25
	} else if gain == Gain_Differential_20_0V {
		res = 20.0
	} else if gain == Gain_Differential_2_0V {
		res = 2.0
	} else if gain == Gain_Differential_2_5V {
		res = 2.5
	} else if gain == Gain_Differential_4_0V {
		res = 4.0
	} else if gain == Gain_Differential_5_0V {
		res = 5.0
	} else if gain == Gain_Unipolar_5_0V {
		res = 5.0
	}
	return res
}

// This command writes the digital port tristate register.  The
// tristate register determines if the latch register value is driven
// onto the port pin.  A '1' in the tristate register makes the
// corresponding pin an input, a '0' makes it an output.  The power
// on default is all input. This board only supports setting the
// entire port as input or output.

// port:   the port to set (0 = Port A, 1 = Port B)
func Digital_tristate_write(value byte, port byte) error {

	logger.Println("call Digital_tristate_write: ", string(value), ",", string(port) )
	var request_type byte = 0x0 << 7 | 0x2 << 5 | 0x0

	var wValue byte = value & 0xff
    var wIndex byte = port  & 0xff

	data := []byte{request_type, byte(dtristate), wValue, wIndex, 0, 0x07, 0xd0}
	_, err := hid_dev.Write(data)
	
	return err
}

func Digital_latch_write(value byte, port byte) error {
    
	logger.Println("call Digital_latch_write: ", string(value), ",", string(port) )
	var request_type byte = 0x0 << 7 | 0x2 << 5 | 0x0
	var wValue byte = value & 0xff
    var wIndex byte = port  & 0xff
	// var delay = 2000 (0x07d0)
	
	data := []byte{request_type, byte(dlatch), wValue, wIndex, 0, 0x07, 0xd0}
	_, err := hid_dev.Write(data)
	
	return  err
}