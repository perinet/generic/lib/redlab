# Redlab changelog

## Upstream
* Add lib package redlab_1208fs_plus

## 0.0.2

* Add license header
* API: Add total number of channels
* Fix: Voltage calculation in single ended mode

## 0.0.1

* 1208LS: Analog in

