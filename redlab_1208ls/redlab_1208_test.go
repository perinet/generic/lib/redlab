package redlab_1208ls

import (
	"testing"
)

func TestMain(t *testing.T) {
	Start()
	res, err := Analog_in(Channel_0, Gain_Differential_4_0V)
	if err != nil {
		logger.Println(err)
		return
	}
	logger.Printf("Channel %d: %f", Channel_0, res)

	res, err = Analog_in(Channel_1, Gain_Differential_4_0V)
	if err != nil {
		logger.Println(err)
		return
	}
	logger.Printf("Channel %d: %f", Channel_1, res)
}
