/*
 * Copyright (c) 2018-2023 Perinet GmbH
 * All rights reserved
 *
 * This software is dual-licensed: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 as
 * published by the Free Software Foundation. For the terms of this
 * license, see http://www.fsf.org/licensing/licenses/agpl-3.0.html,
 * or contact us at https://server.io/contact/ when you want license
 * this software under a commercial license.
 */

// Package redlab_1208ls implements USB HID interface to Redlab 1208LS
// measurement device (https://www.meilhaus.de/en/redlab-1208.htm).
// Reference python implementation: https://github.com/wjasper/Linux_Drivers.git
package redlab_1208ls

import (
	"log"
	"time"

	"github.com/sstallion/go-hid"
)

// Channel to be used by measurement
type Channel byte

// Gain to be used by measurement
type Gain byte

var (
	logger  log.Logger  = *log.Default()
	hid_dev *hid.Device = nil
)

const (
	// Cannels
	Channel_0 Channel = 0
	Channel_1 Channel = 1
	Channel_2 Channel = 2
	Channel_3 Channel = 3
	Channel_4 Channel = 4
	Channel_5 Channel = 5
	Channel_6 Channel = 6
	Channel_7 Channel = 7

	// Total number of channels
	Channel_Number uint = 8

	// Gain Ranges
	Gain_SingelEnded_10_0V  Gain = 0x08 // Single Ended +/- 10.0 V
	Gain_Differential_20_0V Gain = 0x00 // Differential +/- 20.0 V
	Gain_Differential_10_0V Gain = 0x10 // Differential +/- 10.0 V
	Gain_Differential_5_0V  Gain = 0x20 // Differential +/- 5.00 V
	Gain_Differential_4_0V  Gain = 0x30 // Differential +/- 4.00 V
	Gain_Differential_2_5V  Gain = 0x40 // Differential +/- 2.50 V
	Gain_Differential_2_0V  Gain = 0x50 // Differential +/- 2.00 V
	Gain_Differential_1_25V Gain = 0x60 // Differential +/- 1.25 V
	Gain_Differential_1_0V  Gain = 0x70 // Differential +/- 1.00 V

	// USB IDs
	vid uint16 = 0x09db
	pid uint16 = 0x007a

	// Commands
	reset     byte = 0x11
	analog_in byte = 0x06
)

// Reset the measurement device
func Reset() {
	data := []byte{reset, 0, 0, 0, 0, 0, 0, 0}
	hid_dev.Write(data)
}

// Measure an analog in channel
//
// Parameters:
//   - channel: The channel to be measured (e.g. Channel_0)
//   - gain: The gain of the measurement (e.g. Gain_Differential_4_0V for a 3.3V max measurement)
//
// Returns the voltage of the measurement
func Analog_in(channel Channel, gain Gain) (float64, error) {
	data := []byte{analog_in, byte(channel), byte(gain), 0, 0, 0, 0, 0}
	_, err := hid_dev.Write(data)
	if err != nil {
		return 0.0, err
	}

	read_data := make([]byte, 3)
	_, err = hid_dev.ReadWithTimeout(read_data, time.Millisecond*100)
	if err != nil {
		return 0.0, err
	}

	var value float64
	var volts float64
	if gain == Gain_SingelEnded_10_0V {
		value = float64(uint16(read_data[1])<<4|uint16(read_data[0])&0x0f) - 0x400
		volts = float64(value) * factor(gain) / 0x3ff
	} else {
		value = float64(int16((uint16(read_data[0])<<4 | uint16(read_data[1])<<8))) / 16
		volts = float64(value) * factor(gain) / 0x7ff
	}
	return volts, nil
}

// Prepare dependencies for measurement
func Start() {
	logger.SetPrefix("redlab: ")
	logger.Println("starting")

	err := hid.Init()
	if err != nil {
		log.Fatal(err)
	}

	hid_dev, err = hid.OpenFirst(vid, pid)
	if err != nil {
		log.Fatal(err)
	}
}

// Cleanup after measurements has been finished
func Stop() error {
	return hid_dev.Close()
}

func factor(gain Gain) float64 {
	var res float64 = 0.0

	if gain == Gain_Differential_10_0V {
		res = 10.0
	} else if gain == Gain_Differential_1_0V {
		res = 1.0
	} else if gain == Gain_Differential_1_25V {
		res = 1.25
	} else if gain == Gain_Differential_20_0V {
		res = 20.0
	} else if gain == Gain_Differential_2_0V {
		res = 2.0
	} else if gain == Gain_Differential_2_5V {
		res = 2.5
	} else if gain == Gain_Differential_4_0V {
		res = 4.0
	} else if gain == Gain_Differential_5_0V {
		res = 5.0
	} else if gain == Gain_SingelEnded_10_0V {
		res = 10.0
	}
	return res
}
